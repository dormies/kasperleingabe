﻿using UnityEngine;
using System.Collections;
using System.IO.Ports;

public class InputControllerFail : MonoBehaviour
{
		public float speed;
		private float amountToMove;
		SerialPort sp;
		public bool isEnabled;
	public int messung=0;
		//accel
		public int ax;
		public int ay;
		public int az;
		public int axold;
		public int ayold;
		public int azold;
		public int axdiff;
		public int aydiff;
		public int azdiff;
		public int axglobal;
		public int ayglobal;
		public int azglobal;
		//gyro
		public int gx;
		public int gy;
		public int gz;
		public int gxold;
		public int gyold;
		public int gzold;
		public int gxglobal;
		public int gyglobal;
		public int gzglobal;
		
	public float verschiebung = 0f;
	

		void Start ()
		{
				if (isEnabled) {
						sp = new SerialPort ("COM3", 9600);
						Debug.Log ("Created SerialPort");
				}
		}
	
		void Update ()
		{
				if (isEnabled) {

				if ((axglobal > 16383) || (Input.GetKey ("left"))) {
				if ((transform.position.x+verschiebung > -7.3) && (gyglobal>-5000) && (gyglobal<5000)) {
					//transform.position = new Vector3 (transform.position.x - 3.2f, transform.position.y, transform.position.z);
					verschiebung=verschiebung-3.2f;
					axglobal= axglobal-32768;
				}
			}
				if ((axglobal > 8191) ) {
				if ((transform.position.x+verschiebung > -7.3) && (gyglobal>-5000) && (gyglobal<5000)) {
					//transform.position = new Vector3 (transform.position.x - 1.6f, transform.position.y, transform.position.z);
					verschiebung=verschiebung-1.6f;
					axglobal= axglobal-16384;
				}
			}
				if ((axglobal > 4095) ) {
				if ((transform.position.x+verschiebung > -7.3) && (gyglobal>-5000) && (gyglobal<5000)) {
				//transform.position = new Vector3 (transform.position.x - 0.8f, transform.position.y, transform.position.z);
					verschiebung=verschiebung-0.8f;
					axglobal= axglobal-8192;
				}
			}
				if ((axglobal > 2047) ) {	
				if ((transform.position.x+verschiebung > -7.3) && (gyglobal>-5000) && (gyglobal<5000)) {
				//transform.position = new Vector3 (transform.position.x - 0.4f, transform.position.y, transform.position.z);
					verschiebung=verschiebung-0.4f;
					axglobal= axglobal-4096;
				}
			}


			
				if ((axglobal < -16384) || (Input.GetKey ("left"))) {
				if ((transform.position.x+verschiebung < 7.3) && (gyglobal>-5000) && (gyglobal<5000)){
								//transform.position = new Vector3 (transform.position.x + 3.2f, transform.position.y, transform.position.z);
					verschiebung=verschiebung+3.2f;			
					axglobal = axglobal + 32768;
						}
				}
				if ((axglobal < -8192)) {
				if ((transform.position.x+verschiebung < 7.3) && (gyglobal>-5000) && (gyglobal<5000)){
					//transform.position = new Vector3 (transform.position.x + 1.6f, transform.position.y, transform.position.z);
					verschiebung=verschiebung+1.6f;
					axglobal= axglobal+16384;
				}
			}
				if ((axglobal < -4096)) {
				if ((transform.position.x+verschiebung < 7.3) && (gyglobal>-5000) && (gyglobal<5000)){
				//transform.position = new Vector3 (transform.position.x + 0.8f, transform.position.y, transform.position.z);
					verschiebung=verschiebung+0.8f;
					axglobal= axglobal+8192;
				}
			}
				if((axglobal < -2048)) {
				if ((transform.position.x+verschiebung < 7.3) && (gyglobal>-5000) && (gyglobal<5000)){
					//transform.position = new Vector3 (transform.position.x + 0.4f, transform.position.y, transform.position.z);
					verschiebung=verschiebung+0.4f;
					axglobal= axglobal+4096;
					}	
			}
			if ((transform.position.x+verschiebung < 7.3)&&(transform.position.x+verschiebung>-7.3)){
			transform.position = new Vector3 (transform.position.x + verschiebung, transform.position.y, transform.position.z);
			}
			if((axglobal<-50000) || (axglobal>50000)){
				axglobal=0;
			}

						/*if ((axdiff < -5000) || (Input.GetKey ("left"))) {
								if (transform.position.x > -7.3) {
										transform.Rotate (Vector3.up * 90); 
										//if (GameObject.Find ("MausStehundLauf").GetComponent<Animator> ().animation.IsPlaying ("MausStehundLauf")) {
										//Debug.Log("HI");							
										transform.position = new Vector3 (transform.position.x - 1, transform.position.y, transform.position.z);
										//	}
										transform.Rotate (Vector3.down * 90);
								}
						} else if ((axdiff > 5000) || (Input.GetKey ("right"))) {
								if (transform.position.x < 7.3) {
										transform.Rotate (Vector3.down * 90);
										transform.position = new Vector3 (transform.position.x + 1, transform.position.y, transform.position.z);
										transform.Rotate (Vector3.up * 90);		
								}
						}*/
			if (gzglobal>32767){
				transform.Rotate (Vector3.up * 90);
				gzglobal=gzglobal-65536;
			}
			if (gzglobal<-32768){
				transform.Rotate (Vector3.down * 90);
				gzglobal = gzglobal+65536;
			}

						//transform.position = new Vector3 (transform.position.x + axdiff / 10000, transform.position.y, transform.position.z);
						/*Vector3 moveDirection = new Vector3 (gxdiff, gydiff, gzdiff);
		transform.TransformDirection (moveDirection);*/
						/*if (gydiff > 100) {
						transform.Rotate (0, 1, 0);
				} else if (gydiff < -100) {
			transform.Rotate (0, -1, 0);
				}*/

						axold = ax;
						ayold = ay;
						azold = az;
						gxold = gx;
						gyold = gy;
						gzold = gz;
						if (!sp.IsOpen) {
								sp.Open ();
								Debug.Log ("Opened SerialPort");
						} else {
								//Debug.Log (sp.ReadLine());
								if (sp.ReadByte () == 0) {
				
								} else {
										string line = sp.ReadLine ();
										if (line.StartsWith ("ccel")) {
												string[] accel = line.Split (' ');
												string sax = accel [2];
												string say = accel [3];
												string saz = accel [4];
												/*Debug.Log (sax);
					Debug.Log (say);
					Debug.Log (saz);*/
												if (System.Int32.TryParse (sax, out ax)) {
														//Debug.Log (ax);
												}
												if (System.Int32.TryParse (say, out ay)) {
														//Debug.Log (ay);
												}
												if (System.Int32.TryParse (saz, out az)) {
														//Debug.Log (az);
												}
												axdiff = axold - ax;
												aydiff = ayold - ay;
												azdiff = azold - az;

										} else if (line.StartsWith ("yro")) {
												string[] gyro = line.Split (' ');
												string sgx = gyro [3];
												string sgy = gyro [4];
												string sgz = gyro [5];
												/*Debug.Log (sgx);
					/*Debug.Log (sgy);
					Debug.Log (sgz);*/
												if (System.Int32.TryParse (sgx, out gx)) {
														//Debug.Log (gx);
												}
												if (System.Int32.TryParse (sgy, out gy)) {
														//Debug.Log (gy);
												}
												if (System.Int32.TryParse (sgz, out gz)) {
														//Debug.Log (gz);
												}
						gxglobal = gxglobal + gx;
						gyglobal = gyglobal + gy;
						gzglobal = gzglobal + gz;
						if ((gyglobal>-5000) && (gyglobal<5000)){
						axglobal = axglobal + ax;
						ayglobal = ayglobal + ay;
						azglobal = azglobal + az;
						}
						messung++;
										}
								}
						}

				}
		}
}