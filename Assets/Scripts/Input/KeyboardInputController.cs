﻿using UnityEngine;
using System.Collections;


/*
 * Keyboard controls for debug purposes
 * 
 */ 
public class KeyboardInputController : MonoBehaviour {
	
		private Rigidbody characterRigidbody;
		public bool isEnabled = true;

		void Start() {
				if(isEnabled) {
						characterRigidbody = GetComponent<Rigidbody>();						
				}
		}
	
		// Update is called once per frame
		void Update() {
				if(isEnabled) {
						if(Input.GetKey(KeyCode.LeftArrow)) {
								characterRigidbody.velocity = new Vector3(-3, 0, 0);
						} else if(Input.GetKey(KeyCode.RightArrow)) {
								characterRigidbody.velocity = new Vector3(3, 0, 0);
						} else {
								characterRigidbody.velocity = Vector3.zero;
						}
				}
		}
}
