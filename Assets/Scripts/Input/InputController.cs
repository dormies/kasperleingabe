﻿using UnityEngine;
using System.Collections;
using System.IO.Ports;

public class InputController : MonoBehaviour
{
		public float speed;
		private float amountToMove;
		SerialPort sp;
		public bool isEnabled;
		
		Transform gegenstand;

		public int druck;
		
		//accel
		public int ax;
		public int ay;
		public int az;
		public int axold;
		public int ayold;
		public int azold;
		public int axdiff;
		public int aydiff;
		public int azdiff;
		public int axgesamt;
		public int aygesamt;
		public int azgesamt;
		//gyro
		public int gx;
		public int gy;
		public int gz;
		public int gxold;
		public int gyold;
		public int gzold;
		public int gxgesamt;
		public int gygesamt;
		public int gzgesamt;

		public bool jumpAnimation = false;
	

		void Start ()
		{
				if (isEnabled) {
						sp = new SerialPort ("COM3", 9600);
						Debug.Log ("Created SerialPort");
				}
		}
	
		void Update ()
		{
		gegenstand = GameObject.Find("Item").transform;
				
		if (isEnabled) {
			if(!jumpAnimation){
			if (((ax > 3047)&&(az>12000)) || (Input.GetKey ("left"))) {
				if (transform.position.x > -11) {
						this.GetComponent<Animator>().Play("a_schritt");
					transform.position = new Vector3 (transform.position.x - 0.2f, transform.position.y, transform.position.z);
						if (druck>0){
							//TODO: Gegenstand erhält gleichen Transform wie Character
							if (gegenstand.position.x > -11) {
								gegenstand.position = new Vector3 (gegenstand.position.x - 0.2f, gegenstand.position.y, gegenstand.position.z);
           				}
					}
				}
			}else if (((ax < -3048)&&(az>12000)) || (Input.GetKey ("right"))) {
				if (transform.position.x < 11) {
						this.GetComponent<Animator>().Play("a_schritt");
					transform.position = new Vector3 (transform.position.x + 0.2f, transform.position.y, transform.position.z);
						if (druck>0){
							//TODO: Gegenstand erhält gleichen Transform wie Character
							if (gegenstand.position.x < 11) {
								gegenstand.position = new Vector3 (gegenstand.position.x + 0.2f, gegenstand.position.y, gegenstand.position.z);
							}
						}
					}

			}
				if (gzgesamt>32767){
					transform.Rotate (Vector3.up * 90);
					gzgesamt=gzgesamt-65536;
				}
				if (gzgesamt<-32768){
					transform.Rotate (Vector3.down * 90);
					gzgesamt = gzgesamt+65536;
				}
			}

			if((!jumpAnimation) && ((az > 25000)||(Input.GetKey ("up")))){
				jumpAnimation=true;
				this.GetComponent<Animator>().Play("a_jump");
				//animation.Play();
				//TODO: Hier die Animation.
				//Nach Animation: jumpAnimation=false;
				jumpAnimation=false;
			}


						

						axold = ax;
						ayold = ay;
						azold = az;
						gxold = gx;
						gyold = gy;
						gzold = gz;
						if (!sp.IsOpen) {
								sp.Open ();
								Debug.Log ("Opened SerialPort");
						} else {
								//Debug.Log (sp.ReadLine());
								if (sp.ReadByte () == 0) {
				
								} else {
										string line = sp.ReadLine ();
										if (line.StartsWith ("ccel")) {
												string[] accel = line.Split (' ');
												string sax = accel [2];
												string say = accel [3];
												string saz = accel [4];
												/*Debug.Log (sax);
					Debug.Log (say);
					Debug.Log (saz);*/
												if (System.Int32.TryParse (sax, out ax)) {
														//Debug.Log (ax);
												}
												if (System.Int32.TryParse (say, out ay)) {
														//Debug.Log (ay);
												}
												if (System.Int32.TryParse (saz, out az)) {
														//Debug.Log (az);
												}
												axdiff = axold - ax;
												aydiff = ayold - ay;
												azdiff = azold - az;

										} else if (line.StartsWith ("yro")) {
												string[] gyro = line.Split (' ');
												string sgx = gyro [3];
												string sgy = gyro [4];
												string sgz = gyro [5];
												/*Debug.Log (sgx);
					/*Debug.Log (sgy);
					Debug.Log (sgz);*/
												if (System.Int32.TryParse (sgx, out gx)) {
														//Debug.Log (gx);
												}
												if (System.Int32.TryParse (sgy, out gy)) {
														//Debug.Log (gy);
												}
												if (System.Int32.TryParse (sgz, out gz)) {
														//Debug.Log (gz);
												}


						if((gx<-1000)||(gx>1000)){
						gxgesamt = gxgesamt + gx;
						}
						gygesamt = gygesamt + gy;
						gzgesamt = gzgesamt + gz;
						axgesamt = axgesamt + ax;

										} else if (line.StartsWith("ruck")) {
						string[] druckStringArray = line.Split (' ');
						string druckString = druckStringArray [1];
						if (System.Int32.TryParse (druckString, out druck)) {
						}
					}

								}
						}

				}
		}
}