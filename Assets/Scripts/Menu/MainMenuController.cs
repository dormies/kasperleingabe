﻿using UnityEngine;
using System.Collections;
using System.IO;

public class MainMenuController : MonoBehaviour {
	//position stuff
	int halfWidth;
	int halfHeight;
	//menu modes
	bool mainMenu = true;
	bool loadMenu = false;
	//layout
	public GUISkin skin;
	//data loading
	DirectoryInfo dataDirectory;
	private float vScrollbarValue;
	public Vector2 scrollPosition = Vector2.zero;
	public bool boolMouseOver = false ;
	public bool boolMouseOver1 = false ;
	public bool boolMouseOver2 = false ;
	
	
	
	void OnMouseOver() // This Function for  Mouse Over a Mesh or GUIContent
	{
		
		boolMouseOver = true;
		boolMouseOver1 = true;
		boolMouseOver2 = true;
		
	}
	
	
	void OnMouseExit() // This Function for  Mouse Not Over a Mesh or GUIContent
	{
		
		boolMouseOver = false;
		boolMouseOver1 = false;
		boolMouseOver2 = false;
		
	}
	
	void Start() {
		dataDirectory = new DirectoryInfo(Application.dataPath + "/Data/");
	}
	
	void Update() {
		halfWidth = Screen.currentResolution.width / 2;
		halfHeight = Screen.currentResolution.height / 2;
	}
	
	void OnGUI() {
		
		
		GUI.skin = skin;
		if(mainMenu) {
			GUI.Box(new Rect(Screen.width / 2 - 100, Screen.height / 2 - 150, 200, 50),  "Kasperleingabe");
			if(GUI.Button(new Rect(Screen.width / 2 - 100, Screen.height / 2 - 100, 200, 50), new GUIContent("Neue Szene", "Mouse Over Neue Szene"))) {
				
				PlayerPrefs.SetString("activeScene", "");
				Application.LoadLevel("SceneSettingsMenu");			
			}	
			
			if(GUI.Button(new Rect(Screen.width / 2 - 100, Screen.height / 2 - 40, 200, 50), new GUIContent ("Szene laden","Mouse Over Szene laden"))) {
				mainMenu = false;
				loadMenu = true;
			}
			if(GUI.Button(new Rect(Screen.width / 2 - 100, Screen.height / 2 + 20, 200, 50), new GUIContent ("Beenden","Mouse Over Beenden"))) {
				Application.Quit();
			}
			//if(GUI.tooltip =="Mouse Over Neue Szene"||GUI.tooltip =="Mouse Over Szene laden"||GUI.tooltip =="Mouse Over Beenden")
			if(GUI.tooltip =="Mouse Over Neue Szene")
			{
				boolMouseOver = true;
				boolMouseOver1 = false;
				boolMouseOver2 = false;
			}
			else
			{
				boolMouseOver = false;
				
			}
			if(GUI.tooltip =="Mouse Over Szene laden")
			{
				boolMouseOver = false;
				boolMouseOver1 = true;
				boolMouseOver2 = false;
			}
			else
			{
				boolMouseOver1 = false;
				
			}
			if(GUI.tooltip =="Mouse Over Beenden")
			{
				boolMouseOver = false;
				boolMouseOver1 = false;
				boolMouseOver2 = true;
			}
			else
			{
				boolMouseOver2 = false;
				
			}
			
			if(boolMouseOver == true)
			{
				GUI.Box (new Rect(Screen.width / 2 + 120, Screen.height / 2 - 85,250,100) ,"Erstelle eine neue Szene","beschreibung");
				
			}
			if(boolMouseOver1 == true)
			{
				GUI.Box (new Rect(Screen.width / 2 + 120, Screen.height / 2 - 25,250,100) ,"Eine erstellte Szene Laden","beschreibung");
				
			}
			if(boolMouseOver2 == true)
			{
				GUI.Box (new Rect(Screen.width / 2 + 120, Screen.height / 2 + 35,250,100) ,"Das Programm schließen","beschreibung");
				
			}
		}
		if(loadMenu) {
			//zurück button
			if(GUI.Button(new Rect(Screen.width / 2 - 100, Screen.height / 2 + 120, 200, 50), "Zurück")) {
				loadMenu = false;
				mainMenu = true;
			}
			//Header
			GUI.Box(new Rect(Screen.width / 2 - 100, Screen.height / 2 - 150, 200, 50), "Kasperleingabe");
			//scrollbalken position, scroll bereich
			scrollPosition = GUI.BeginScrollView(new Rect(Screen.width / 2 - 100, Screen.height / 2 - 100,200,200), scrollPosition, new Rect (0,0,0,600));
			//saved scenes
			FileInfo[] info = dataDirectory.GetFiles("*.xml");
			int i = 1;
			foreach(FileInfo f in info) {
				string name = f.Name.Replace(".xml", "");
				//if(GUI.Button(new Rect(Screen.width / 2 - 100, Screen.height / 2 + 80 - i * 60, 200, 50), name))
				if(GUI.Button(new Rect(0,0, 200, 50), name)) {
					PlayerPrefs.SetString("activeScene", name);
					Application.LoadLevel("Scene");
					
					GUI.EndScrollView();
				}
				i++;
			}
			
			
		}
		
	}
}
