﻿using UnityEngine;
using System.Collections;

public class SceneSettingsMenuController : MonoBehaviour {
	//menu modes
	bool bgSelect;
	bool charSelect;
	bool itemSelect;
	//pictures
	public Texture2D[] backgrounds;
	public Texture2D[] characters;
	public Texture2D[] items;
	//data
	private int selectedBackground = 0;
	private int selectedCharacter = 0;
	private int selectedItem = 0;
	//layout
	public GUISkin skin;
	public bool boolMouseOver = false ;
	public bool boolMouseOver1 = false ;
	public bool boolMouseOver2 = false ;
	
	void OnMouseOver() // This Function for  Mouse Over a Mesh or GUIContent
	{
		
		boolMouseOver = true;
		boolMouseOver1 = true;
		boolMouseOver2 = true;
		
	}
	
	
	void OnMouseExit() // This Function for  Mouse Not Over a Mesh or GUIContent
	{
		
		boolMouseOver = false;
		boolMouseOver1 = false;
		boolMouseOver2 = false;
		
	}
	
	void Start() {
		if(PlayerPrefs.HasKey("background")) {
			selectedBackground = PlayerPrefs.GetInt("background");
		}
		if(PlayerPrefs.HasKey("character")) {
			selectedCharacter = PlayerPrefs.GetInt("character");
		}
		if(PlayerPrefs.HasKey("item")) {
			selectedItem = PlayerPrefs.GetInt("item");
		}
	}	
	
	void OnGUI() {
		
		GUI.skin = skin;
		GUI.Box(new Rect(Screen.width / 2 - 100, Screen.height / 2 - 150, 200, 50), "Neue Szene");
		//Background
		if(GUI.Button(new Rect(Screen.width / 2 - 215, Screen.height / 2 - 100, 125, 125), new GUIContent (backgrounds[selectedBackground],"Mouse Over BG Select"))) { 
			charSelect = false;
			itemSelect = false;				
			bgSelect = true;
		}
		//Character
		if(GUI.Button(new Rect(Screen.width / 2 - 65, Screen.height / 2 - 100, 125, 125),new GUIContent  (characters[selectedCharacter],"Mouse Over Char Select"))) {
			itemSelect = false;				
			bgSelect = false;
			charSelect = true;
		}
		//Item
		if(GUI.Button(new Rect(Screen.width / 2 + 85, Screen.height / 2 - 100, 125, 125),new GUIContent (items[selectedItem],"Mouse Over Item Select"))) {
			charSelect = false;
			bgSelect = false;
			itemSelect = true;				
		}
		if(GUI.Button(new Rect(Screen.width / 2 + 230, Screen.height / 2 - 15, 125, 45), "Zufall")) {
			selectedBackground = Random.Range(0, 3);
			selectedCharacter = Random.Range(0, 3);
			selectedItem = Random.Range(0, 3);
		}
		if(GUI.Button(new Rect(Screen.width / 2 + 230, Screen.height / 2 - 100, 125, 45), "Weiter")) {
			PlayerPrefs.SetInt("background", selectedBackground);
			PlayerPrefs.SetInt("character", selectedCharacter);
			PlayerPrefs.SetInt("item", selectedItem);
			Application.LoadLevel("Scene");
		}
		//Background Selection
		if(bgSelect) {
			GUI.Box(new Rect(Screen.width / 2 - 350, Screen.height / 2 + 125, 200f, 60f), "Hintergrund auswählen");
			if(GUI.Button(new Rect(Screen.width / 2 - 150, Screen.height / 2 + 80, 125, 125), backgrounds[0])) {
				selectedBackground = 0;
				bgSelect = false;
			}
			if(GUI.Button(new Rect(Screen.width / 2 - 0, Screen.height / 2 + 80, 125, 125), backgrounds[1])) {
				selectedBackground = 1;
				bgSelect = false;
			}
			if(GUI.Button(new Rect(Screen.width / 2 + 150, Screen.height / 2 + 80, 125, 125), backgrounds[2])) {
				selectedBackground = 2;							
				bgSelect = false;
			}
		}
		//Character Selection
		if(charSelect) {
			GUI.Box(new Rect(Screen.width / 2 - 350, Screen.height / 2 + 125, 200f, 60f), "Figur auswählen");
			if(GUI.Button(new Rect(Screen.width / 2 - 150, Screen.height / 2 + 80, 125, 125), characters[0])) {
				selectedCharacter = 0;
				charSelect = false;
			}
			if(GUI.Button(new Rect(Screen.width / 2 - 0, Screen.height / 2 + 80, 125, 125), characters[1])) {
				selectedCharacter = 1;
				charSelect = false;
			}
			if(GUI.Button(new Rect(Screen.width / 2 + 150, Screen.height / 2 + 80, 125, 125), characters[2])) {
				selectedCharacter = 2;							
				charSelect = false;
			}
		}
		//Item Selection
		if(itemSelect) {
			GUI.Box(new Rect(Screen.width / 2 - 350, Screen.height / 2 + 125, 200f, 60f), "Gegenstand auswählen");
			if(GUI.Button(new Rect(Screen.width / 2 - 150, Screen.height / 2 + 80, 125, 125), items[0])) {
				selectedItem = 0;
				itemSelect = false;
			}
			if(GUI.Button(new Rect(Screen.width / 2 - 0, Screen.height / 2 + 80, 125, 125), items[1])) {
				selectedItem = 1;
				itemSelect = false;
			}
			if(GUI.Button(new Rect(Screen.width / 2 + 150, Screen.height / 2 + 80, 125, 125), items[2])) {
				selectedItem = 2;							
				itemSelect = false;
			}
		}
		if(GUI.tooltip =="Mouse Over BG Select")
		{
			boolMouseOver = true;
			boolMouseOver1 = false;
			boolMouseOver2 = false;
		}
		else
		{
			boolMouseOver = false;
			
		}
		if(GUI.tooltip =="Mouse Over Char Select")
		{
			boolMouseOver = false;
			boolMouseOver1 = true;
			boolMouseOver2 = false;
		}
		else
		{
			boolMouseOver1 = false;
			
		}
		if(GUI.tooltip =="Mouse Over Item Select")
		{
			boolMouseOver = false;
			boolMouseOver1 = false;
			boolMouseOver2 = true;
		}
		else
		{
			boolMouseOver2 = false;
			
		}
		
		if(boolMouseOver == true)
		{
			GUI.Box (new Rect(Screen.width / 2 - 265, Screen.height / 2 + 45,250,100) ,"Wähle einen Hintergrund","beschreibung");
			
		}
		if(boolMouseOver1 == true)
		{
			GUI.Box (new Rect(Screen.width / 2 - 70, Screen.height / 2 +45,250,100) ,"Wähle eine Figur","beschreibung");
			
		}
		if(boolMouseOver2 == true)
		{
			GUI.Box (new Rect(Screen.width / 2 + 40, Screen.height / 2 + 45,250,100) ,"Wähle einen Gegenstand","beschreibung");
			
		}
	}
	
}
