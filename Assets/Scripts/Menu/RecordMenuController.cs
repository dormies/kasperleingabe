﻿using UnityEngine;
using System.Collections;

using UnityEngine;


public class RecordMenuController : MonoBehaviour {
	public GameObject background;
	public GameObject character;
	public GameObject item;
	
	//pictures
	public Texture2D[] backgrounds;
	public Texture2D[] characters;
	public Texture2D[] items;
	
	private bool isRecording = false;
	private bool hasRecorded = false;
	private bool saveDialogue = false;
	private bool loadScene = false;
	
	private int currentBackground;
	private int currentCharacter;
	private int currentItem;
	
	private RecordController recordController;
	private string sceneName = "";
	private float startTime;
	public float hSliderValue = 0.0F;
	
	//layout
	public GUISkin skin;
	public bool boolMouseOver = false ;
	public bool boolMouseOver1 = false ;
	public bool boolMouseOver2 = false ;
	
	
	void OnMouseOver() // This Function for  Mouse Over a Mesh or GUIContent
	{
		
		boolMouseOver = true;
		boolMouseOver1 = true;
		boolMouseOver2 = true;
		
	}
	
	
	void OnMouseExit() // This Function for  Mouse Not Over a Mesh or GUIContent
	{
		
		boolMouseOver = false;
		boolMouseOver1 = false;
		boolMouseOver2 = false;
		
	}
	
	
	void Start() {
		
		background = GameObject.Find("Background");
		character = GameObject.Find("Character");
		item = GameObject.Find("Item");
		recordController = GameObject.Find("Character").GetComponent<RecordController>();
		
		if(background != null)
			character.transform.position += new Vector3(0f, 3.5f, 0f);
		item.transform.position += new Vector3(3f, 5f, 0f);
		
		//LOAD EXISTING SCENE
		if(PlayerPrefs.GetString("activeScene") != "") {
			loadScene = true;
			Debug.Log(PlayerPrefs.GetString("activeScene"));
			recordController.LoadRecording();
		}
		//USE SETTINGS
		if(!loadScene) {
			currentBackground = PlayerPrefs.GetInt("background");
			currentCharacter = PlayerPrefs.GetInt("character");
			currentItem = PlayerPrefs.GetInt("item");
			background.renderer.material.mainTexture = backgrounds[currentBackground];
			//character.renderer.material.mainTexture = characters[currentCharacter];
			item.renderer.material.mainTexture = items[currentItem];
		}
	}
	
	void OnGUI() {
		
		
		GUI.skin = skin;
		if(!isRecording && !hasRecorded && !saveDialogue) {
			GUI.BeginGroup(new Rect(Screen.width / 2 - 200, Screen.height / 1.2f, 400, 60));
			GUI.Box(new Rect(0, 0, 400, 200), "");
			if(GUI.Button(new Rect(40, 10, 100, 40),new GUIContent ("Ändern","Mouse Over Ändern"))) {
				Application.LoadLevel("SceneSettingsMenu");
			}	
			if(GUI.Button(new Rect(150, 10, 100, 40),new GUIContent ("Aufnehmen","Mouse Over Aufnehmen"))) {
				isRecording = true;
				recordController.startTime = Time.time;
				recordController.StartRecording();
			}
			if(GUI.Button(new Rect(260, 10, 100, 40),new GUIContent ("Verlassen","Mouse Over Verlassen"))) {
				Application.LoadLevel("MainMenu");
			}
			GUI.EndGroup();
		}
		if(!isRecording && hasRecorded && !saveDialogue || !isRecording && loadScene && !saveDialogue) {
			//Video Playback
			GUI.BeginGroup(new Rect(Screen.width / 2 - 90, Screen.height / 1.3f, 180, 40));
			GUI.Box(new Rect(0, 0, 400, 200), "");
			hSliderValue = GUI.HorizontalSlider(new Rect(17, -1, 150, 30), hSliderValue, 0.0F, 10.0F);
			//TODO display pause button during plays
			if(!recordController.isPlaying) {
				if(GUI.Button(new Rect(83, 18, 20, 20), "►")) {
					recordController.StartCoroutine("Play");
				}	
			} else {
				if(GUI.Button(new Rect(83, 18, 20, 20), "||")) {
					recordController.StopCoroutine("Play");
					recordController.isPlaying = false;
				}
			}						
			GUI.EndGroup();
			//Main Buttons
			GUI.BeginGroup(new Rect(Screen.width / 2 - 200, Screen.height / 1.2f, 400, 60));
			GUI.Box(new Rect(0, 0, 400, 200), "");
			if(GUI.Button(new Rect(40, 10, 100, 40), new GUIContent ("Ändern","Mouse Over Ändern"))) {
				Application.LoadLevel("SceneSettingsMenu");
			}	
			if(GUI.Button(new Rect(150, 10, 100, 40), new GUIContent ("Aufnehmen","Mouse Over Aufnehmen"))) {
				//isRecording = true;
			}
			if(GUI.Button(new Rect(260, 10, 100, 40), new GUIContent ("Verlassen","Mouse Over Verlassen"))) {
				saveDialogue = true;
			}
			GUI.EndGroup();
		}
		if(isRecording) {
			GUI.BeginGroup(new Rect(Screen.width / 2 - 200, Screen.height / 1.2f, 400, 60));
			GUI.Box(new Rect(0, 0, 400, 200), "");
			if(GUI.Button(new Rect(40, 10, 100, 40), "Stop")) {
				hasRecorded = true;
				isRecording = false;
				recordController.StopRecording();
			}
			GUI.Label(new Rect(150, 10, 100, 40), "" + recordController.currentPlayTime);
			GUI.EndGroup();
		}
		if(saveDialogue) {
			GUI.BeginGroup(new Rect(Screen.width / 2 - 200, Screen.height / 1.2f, 400, 60));
			GUI.Box(new Rect(0, 0, 400, 200), "");
			GUILayout.Label("Name:");
			sceneName = GUI.TextField(new Rect(60, 10, 180, 40), sceneName);
			if(GUI.Button(new Rect(260, 10, 100, 40), "Speichern")) {
				recordController.SaveRecording(sceneName);
				saveDialogue = false;
				Application.LoadLevel("MainMenu");
			}
			GUI.EndGroup();
		}
		if(GUI.tooltip =="Mouse Over Ändern")
		{
			boolMouseOver = true;
			boolMouseOver1 = false;
			boolMouseOver2 = false;
		}
		else
		{
			boolMouseOver = false;
			
		}
		if(GUI.tooltip =="Mouse Over Aufnehmen")
		{
			boolMouseOver = false;
			boolMouseOver1 = true;
			boolMouseOver2 = false;
		}
		else
		{
			boolMouseOver1 = false;
			
		}
		if(GUI.tooltip =="Mouse Over Verlassen")
		{
			boolMouseOver = false;
			boolMouseOver1 = false;
			boolMouseOver2 = true;
		}
		else
		{
			boolMouseOver2 = false;
			
		}
		
		if(boolMouseOver == true)
		{
			GUI.Box (new Rect(Screen.width / 2 - 230, Screen.height / 2 +280,250,100) ,"Änder die jetzige Szene","beschreibung");
			
		}
		if(boolMouseOver1 == true)
		{
			GUI.Box (new Rect(Screen.width / 2 - 80, Screen.height / 2 +280,250,100) ,"Starte die Aufnahme","beschreibung");
			
		}
		if(boolMouseOver2 == true)
		{
			GUI.Box (new Rect(Screen.width / 2 + 40, Screen.height / 2 + 280,250,100) ,"Szene schließen","beschreibung");
			
		}
		
		
	}
}
