﻿using UnityEngine;
using System.Xml;
using System.IO;
using System;
using System.Collections;
using System.Collections.Generic;

public class RecordController : MonoBehaviour {

		private bool isRecording = false;
		public bool isPlaying = false;
		private XmlWriterSettings writerSettings;
		private float tempX;
		private float tempY;
		private float tempXRotation;
		private float tempYRotation;
		private float tempZRotation;
		private float tempWRotation;
		private List<float> xPositions = new List<float>();
		private List<float> yPositions = new List<float>();
		private List<float> xRotations = new List<float>();
		private List<float> yRotations = new List<float>();
		private List<float> zRotations = new List<float>();
		private List<float> wRotations = new List<float>();
		private List<float> timeStamps = new List<float>();
		public Transform characterTransform;
		public float startTime;
		public float currentPlayTime;

		void FixedUpdate() {
				if(isRecording) {
						Invoke("Record", 0f);
				} else {
						CancelInvoke("Record");
				}
		}
		
		public void Record() {
				currentPlayTime = Time.time - startTime;
				tempX = characterTransform.position.x;
				tempY = characterTransform.position.y;
				tempXRotation = characterTransform.rotation.x;
				tempYRotation = characterTransform.rotation.y;
				tempZRotation = characterTransform.rotation.z;
				tempWRotation = characterTransform.rotation.w;
				//inital node
				if(timeStamps.Count == 0) {
						xPositions.Add(tempX);
						yPositions.Add(tempY);
						xRotations.Add(tempXRotation);
						yRotations.Add(tempYRotation);
						zRotations.Add(tempZRotation);
						wRotations.Add(tempWRotation);
						timeStamps.Add(currentPlayTime);
						//check if node at that timestamp exists, if not: write new node
				} else if(timeStamps[timeStamps.Count - 1] != currentPlayTime) {
						xPositions.Add(tempX);
						yPositions.Add(tempY);
						xRotations.Add(tempXRotation);
						yRotations.Add(tempYRotation);
						zRotations.Add(tempZRotation);
						wRotations.Add(tempWRotation);
						timeStamps.Add(currentPlayTime);
				}
		}
	
		IEnumerator Play() {
				isPlaying = true;
				for(int i =0; i<xPositions.Count-1; i++) {
						//set initial position
						if(i == 0) {
								characterTransform.position = new Vector3(xPositions[i], yPositions[i], 0);
						}
						//interpolate from current position to next position node
						Vector3 currentPos = characterTransform.position;
						Vector3 nextPos = new Vector3(xPositions[i + 1], yPositions[i + 1], 0);
						float startTime = timeStamps[i];
						float journeyLength = Vector3.Distance(currentPos, nextPos);
						float distCovered = (Time.time - timeStamps[i]);
						float fracJourney = distCovered / journeyLength;
						characterTransform.position = Vector3.Lerp(currentPos, nextPos, fracJourney);
						//characterTransform.position = new Vector3(xPositions[i], yPositions[i], 0);
						characterTransform.rotation = new Quaternion(xRotations[i], yRotations[i], zRotations[i], wRotations[i]);
						yield return null;
				}
				isPlaying = false;
		}

		IEnumerator MoveTo(Vector3 position, float time) {
				Vector3 start = transform.position;
				Vector3 end = position;
				float t = 0;
				while(t < 1) {
						yield return null;
						t += Time.deltaTime / time;
						transform.position = Vector3.Lerp(start, end, t);
				}
				transform.position = end;
		}
	
		public void SaveRecording(string name) {
				if(!File.Exists(GetFilePath(name))) {
						writerSettings = new XmlWriterSettings();
						writerSettings.Indent = true;
						using(XmlWriter writer = XmlWriter.Create(GetFilePath(name),writerSettings)) {
								writer.WriteStartDocument();
								writer.WriteStartElement("recording");
								writer.WriteAttributeString("name", name);
								writer.WriteStartElement("settings");
								writer.WriteAttributeString("background", PlayerPrefs.GetInt("background") + "");
								writer.WriteAttributeString("character", PlayerPrefs.GetInt("character") + "");
								writer.WriteAttributeString("item", PlayerPrefs.GetInt("item") + "");
								writer.WriteEndElement();
								for(int i=0; i<xPositions.Count; i++) {
										writer.WriteStartElement("position");
										writer.WriteAttributeString("x", "" + xPositions[i]);
										writer.WriteAttributeString("y", "" + yPositions[i]);
										writer.WriteAttributeString("xRot", "" + xRotations[i]);
										writer.WriteAttributeString("yRot", "" + yRotations[i]);
										writer.WriteAttributeString("zRot", "" + zRotations[i]);
										writer.WriteAttributeString("wRot", "" + wRotations[i]);
										writer.WriteAttributeString("time", "" + timeStamps[i]);
										writer.WriteEndElement();
								}
								writer.WriteEndElement();
								writer.WriteEndDocument();
								writer.Close();
								Debug.Log("Neue Aufnahme erstellt: (" + name + ")");
						}
				} else {
						Debug.LogError("Es gibt schon eine Aufnahme mit diesem Namen.");
				}
		}

		public void LoadRecording() {
				int currentBackground;
				int currentCharacter;
				int currentItem;
				float currentX = 0f;
				float currentY = 0f;
				float currentXRot = 0f;
				float currentYRot = 0f;
				float currentZRot = 0f;
				float currentWRot = 0f;
				float currentTime = 0f;
				XmlDocument recordDoc = new XmlDocument();
				string name = PlayerPrefs.GetString("activeScene");
				if(File.Exists(GetFilePath(name))) {
						recordDoc.Load(GetFilePath(name));
						Int32.TryParse(recordDoc.SelectSingleNode("//settings").Attributes.GetNamedItem("background").Value, out currentBackground);
						Int32.TryParse(recordDoc.SelectSingleNode("//settings").Attributes.GetNamedItem("character").Value, out currentCharacter);
						Int32.TryParse(recordDoc.SelectSingleNode("//settings").Attributes.GetNamedItem("item").Value, out currentItem);
						PlayerPrefs.SetInt("background", currentBackground);
						PlayerPrefs.SetInt("character", currentBackground);
						PlayerPrefs.SetInt("item", currentItem);
						XmlNodeList positions = recordDoc.SelectNodes("//position");
						for(int i=0; i<positions.Count; i++) {
								float.TryParse(positions.Item(i).Attributes.GetNamedItem("x").Value, out currentX);
								float.TryParse(positions.Item(i).Attributes.GetNamedItem("y").Value, out currentY);
								float.TryParse(positions.Item(i).Attributes.GetNamedItem("xRot").Value, out currentXRot);
								float.TryParse(positions.Item(i).Attributes.GetNamedItem("yRot").Value, out currentYRot);
								float.TryParse(positions.Item(i).Attributes.GetNamedItem("zRot").Value, out currentZRot);
								float.TryParse(positions.Item(i).Attributes.GetNamedItem("wRot").Value, out currentWRot);
								float.TryParse(positions.Item(i).Attributes.GetNamedItem("time").Value, out currentTime);
								xPositions.Add(currentX);
								yPositions.Add(currentY);
								xRotations.Add(currentXRot);
								yRotations.Add(currentYRot);
								zRotations.Add(currentZRot);
								wRotations.Add(currentWRot);
								timeStamps.Add(currentTime);
						}
						Debug.Log("Aufnahme geladen. (" + name + ")");
				} else {
						Debug.LogError("Fehler beim laden.");
				}
		}

	
		public void StartRecording() {
				isRecording = true;
		}
	
		public void StopRecording() {
				isRecording = false;
		}

		private string GetFilePath(string name) {
				string filePath = Application.dataPath + "/Data/" + name + ".xml";
				return filePath;
		}

}